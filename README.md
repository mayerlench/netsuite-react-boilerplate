# NetSuite React Boilerplate ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

NetSuite React Boilerplate is a setup and production / devleopment guide on how to get react running in NetSuite

### Installation

* Download or clone this repository
* cd into root folder and run `npm i`


## Configuration
1). Rename the .env.sample file to just .env  
2). Add your NetNuite account and restlet info as shown below (Add restlet relative paths)
```
REACT_APP_NSACCOUNT=988765_SB1
REACT_APP_NSEMAIL=email@gmail.com
REACT_APP_NSPASSWORD=flubberpassword
REACT_APP_NSROLE=3
REACT_APP_RESTLET=/app/site/hosting/restlet.nl?script=1863&deploy=1
GENERATE_SOURCEMAP=false
```  

3). In the package.json change the proxy to point to your netsuite account. This should be the root domain of your restlet link
`"proxy": "https://988765-sb1.restlets.api.netsuite.com"`  

## What is setup for you
1). axios default headers for development in the index.js file.

```
import axios from 'axios'

if (process.env.NODE_ENV === 'development') {
    const { REACT_APP_NSEMAIL, REACT_APP_NSPASSWORD, REACT_APP_NSACCOUNT,REACT_APP_NSROLE } = process.env

    axios.defaults.headers = {
        'Content-Type': 'application/json',
        Authorization: `NLAuth nlauth_account=${REACT_APP_NSACCOUNT}, nlauth_email=${REACT_APP_NSEMAIL}, nlauth_signature=${REACT_APP_NSPASSWORD}, nlauth_role=${REACT_APP_NSROLE}`
    }
}
```
2). The webpack.config.js and the package.json script builds / proxying
```
"build": "npm run build:react && npm run build:bundle",
"build:react": "react-scripts build",
"build:bundle": "webpack --config webpack.config.js"
```  


## Netsuite Setup
Setting up in netsuite will consist of the following:   
* Restlet - The relative path in the .env file   
* Suitelet - To serve the react app (Not required to develop locally)      


```js
/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */
define(['N/runtime'],
    function (runtime) {

        return {
            get: function _get(c) {
            return JSON.stringify(runtime.getCurrentUser())
        }
     }
});

/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */
define(['N/file', 'N/ui/serverWidget', 'N/runtime', 'N/url', 'SuiteScripts/DMLibs/thirdParty/ramda'],
    function (file, ui, runtime, url, R) {

        function onRequest(context) {
            var response = context.response
            var request = context.request

            var form = ui.createForm({ title: ' ' })
            var bodyAreaField = form.addField({
                id: 'custpage_bodyareafield',
                type: ui.FieldType.INLINEHTML,
                label: 'Body Area Field'
            });

            if (request.parameters.iframe !== 'T') {
                var scriptObj = runtime.getCurrentScript();
                var scriptUrl = url.resolveScript({
                    scriptId: scriptObj.id,
                    deploymentId: scriptObj.deploymentId,
                })
                bodyAreaField.defaultValue = '<iframe src="' + scriptUrl + '&iframe=T" style="display: block; height: 73vh; width: 100%; border: none;"></iframe>'
                response.writePage(form)
            }
            else {
                var script = file.load({ id: 'SuiteScripts/ReactTests/bundle.min.js' }).url
                var html = '<!DOCTYPE html>' +
                    '<html lang="en">' +
                    '<head>' +
                    '<meta charset="utf-8">' +
                    '<title>Netsuite React</title>' +
                    '</head>' +
                    '<body>' +
                    '<div id="root"></div>' +
                    '<script type="text/javascript" src="' + script + '"></script>' +
                    '</body>' +
                    '</html>'
                response.write(html)
            }
        }

        return {
            onRequest: onRequest
        }
    })
```  

##Development  
Run your app `npm run`
Start developing

## Creating a build for Netsuite
Create a build by running `npm run build`  
This will run react-scripts. Webpack will then bundle you entire app into one bundle.min.js file  
All thats left to do is serve this one js file in a suitelet like shown above.  


##Caveats
Routing:  
A suitelet url looks like this `/app/site/hosting/scriptlet.nl?script=1848&deploy=1&compid=795689&whence=`  
Because the url already has its own query parameters you cannot use Reacts BrowserRouter.   
Instead use Reacts built in HashRouter. Your routes will then use a hash and look like this `/app/site/hosting/scriptlet.nl?script=1848&deploy=1&compid=795689&whence=#/myReactRoute`

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)