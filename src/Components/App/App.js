import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import logo from './logo.svg'
import './App.css'


class App extends Component {

  state = { user: null }

  componentWillMount() {
    this.setState({ user: false })
    axios.get(process.env.REACT_APP_RESTLET).then(response => {
      this.setState({ user: response.data })
      console.log(response.data)
    }).catch(e => {
      this.setState({ user: null })
      console.log(e)
    })
  }

  renderUser() {
    if (!this.state.user == null)
      return (<p className="App-intro">
        Error: could not get user info
              </p>)

    if (!this.state.user)
      return (<p className="App-intro">
        Fetching your info....
        </p>)

    const { id, name, email, roleCenter, roleId } = this.state.user

    const firstName = name.split(',')[1]
    const lastName = name.split(',')[0]

    return (
      <div className="jumbotron text-primary">
        <p className="App-intro">Hello, {firstName} {lastName}</p>
        <p className="App-intro">Email: {email}</p>
        <p className="App-intro">Internal Id: {id}</p>
        <p className="App-intro">Center: {roleCenter}</p>
        <p className="App-intro">Role: {roleId}</p>
      </div>
    )
  }

  render() {
    return (
      <div className="container text-center">
        <header className="App-header">
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png" className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React - NETSUITE</h1>
        </header>
        {this.renderUser()}
      </div>
    );
  }
}

export default App;
