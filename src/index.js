import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'
import './Main/index.css';
import App from './Components/App/App'
//import registerServiceWorker from './Main/registerServiceWorker';

if (process.env.NODE_ENV === 'development') {
    const { REACT_APP_NSEMAIL, REACT_APP_NSPASSWORD, REACT_APP_NSACCOUNT,REACT_APP_NSROLE } = process.env

    axios.defaults.headers = {
        'Content-Type': 'application/json',
        Authorization: `NLAuth nlauth_account=${REACT_APP_NSACCOUNT}, nlauth_email=${REACT_APP_NSEMAIL}, nlauth_signature=${REACT_APP_NSPASSWORD}, nlauth_role=${REACT_APP_NSROLE}`
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
//registerServiceWorker();
